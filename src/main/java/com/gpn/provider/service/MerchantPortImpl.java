package com.gpn.provider.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.gpn.provider.model.CheckRequest;
import com.gpn.provider.model.CheckResponse;

@WebService(name = "MerchantPort", targetNamespace = "http://www.example.org/Merchant/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
//@BindingType("https://www.w3.org/2010/soapjms/")
@XmlSeeAlso({ObjectFactory.class})
public class MerchantPortImpl implements MerchantPort {

	@Override
	@WebMethod(action = "http://www.example.org/Merchant/validateCheck")
    @WebResult(name = "checkResponse", targetNamespace = "http://www.example.org/Merchant/", partName = "parameters")
    public CheckResponse validateCheck(
        @WebParam(name = "checkRequest", targetNamespace = "http://www.example.org/Merchant/", partName = "parameters")
        CheckRequest parameters)	{
		
		CheckResponse response = new CheckResponse();
		response.setOut("Team Lunch");
		
		return response;
	}
}
