package com.gpn.provider;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.Endpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.JmsListener;
import org.xml.sax.SAXException;

import com.gpn.provider.service.MerchantPortImpl;

@SpringBootApplication
public class SoapProducerApplication {

	
	public static void main(String[] args) {

		Endpoint ep = Endpoint.create(new MerchantPortImpl());
		ep.publish("http://localhost:8081/spring-boot-provider-soap-web-services/Merchant");

		//File file = new File(SoapProducerApplication.class.getClassLoader().getResource("Services/RecherchePoint-v2.0.wsdl").getFile());
		
		System.out.println("Service is up check wsdl, Springboot starting...");

		SpringApplication app = new SpringApplication(SoapProducerApplication.class);
		app.run(args);
	}
	
	@JmsListener(destination = "test.req.queue")
	public void receiveMessage(final Message message) throws JMSException, ParserConfigurationException, FileNotFoundException, SAXException, IOException {

		String messageData = null;
		System.out.println("Received message-> " + message);
		
		/*
		 * final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		 * dbf.setNamespaceAware(true); dbf.setValidating(true);
		 * 
		 * final DocumentBuilder db = dbf.newDocumentBuilder(); final MyResolver r =
		 * MyResolver.newInstance(); db.setEntityResolver(r);
		 * 
		 * String msgBody = ((TextMessage) message).getText(); InputStream is = new
		 * ByteArrayInputStream(msgBody.getBytes());
		 * 
		 * // or final Document docFromFile = db.parse(is); //final Document
		 * docFromStream = db.parse(is);
		 * 
		 * try {
		 * 
		 * XMLInputFactory xif = XMLInputFactory.newFactory(); XMLStreamReader xsr =
		 * xif.createXMLStreamReader(is);
		 * 
		 * //xsr.nextTag(); // Advance to Envelope tag
		 * 
		 * //xsr.nextTag(); // Advance to Body tag //xsr.nextTag(); //xsr.nextTag();
		 * 
		 * 
		 * JAXBContext jc = JAXBContext.newInstance(AddNumbers.class); Unmarshaller
		 * unmarshaller = jc.createUnmarshaller(); JAXBElement<AddNumbers> je =
		 * unmarshaller.unmarshal(xsr, AddNumbers.class);
		 * 
		 * System.out.println(je.getName()); System.out.println(je.getValue());
		 * //System.out.println(je.getValue().getDetails());
		 * 
		 * } catch (XMLStreamException e) { e.printStackTrace(); } catch (JAXBException
		 * e) { e.printStackTrace(); }
		 */
		
		if (message instanceof TextMessage) {
			TextMessage textMessage = (TextMessage) message;
			messageData = textMessage.getText();
		
			System.out.println("Message data-> " + messageData);
		}
	}
}