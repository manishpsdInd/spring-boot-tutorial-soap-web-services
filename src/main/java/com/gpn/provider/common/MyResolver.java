package com.gpn.provider.common;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

public class MyResolver implements EntityResolver {
	
	@Override
    public InputSource resolveEntity (String publicId, String systemId) {
        if (systemId.equals("http://localhost:8081/spring-boot-tutorial-soap-web-services/productservice?wsdl")) {
            
        	// return a special input source
            String reader = null;
            
			return new InputSource(reader);
        } else {
            // use the default behaviour
            return null;
        }
    }

	public static MyResolver newInstance() {
		return new MyResolver();
	}
}