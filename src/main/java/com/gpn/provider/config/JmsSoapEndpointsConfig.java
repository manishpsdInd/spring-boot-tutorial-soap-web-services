package com.gpn.provider.config;

import javax.jms.ConnectionFactory;
import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.jms.JMSConfigFeature;
import org.apache.cxf.transport.jms.JMSConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gpn.provider.service.MerchantPortImpl;

@Configuration
public class JmsSoapEndpointsConfig {

	@Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus(){
        SpringBus bus = new SpringBus();
        return bus;
    }
	
    @Bean
    public Endpoint jmsEndpoint(SpringBus bus, JMSConfigFeature jmsConfigFeature) {
        EndpointImpl endpoint = (EndpointImpl) new EndpointImpl(bus, new MerchantPortImpl());
        endpoint.getFeatures().add(jmsConfigFeature);
        endpoint.publish("jms://");
        return endpoint;
    }
    
    @Bean
    public JMSConfigFeature jmsConfigFeature(ConnectionFactory mqConnectionFactory){
        JMSConfigFeature feature = new JMSConfigFeature();

        JMSConfiguration jmsConfiguration = new JMSConfiguration();
        jmsConfiguration.setConnectionFactory(mqConnectionFactory);
        jmsConfiguration.setTargetDestination("test.req.queue");
        jmsConfiguration.setMessageType("text");

        feature.setJmsConfig(jmsConfiguration);
        return feature;
    }
}    